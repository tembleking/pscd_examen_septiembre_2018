

build:
	mkdir build
	cd build && cmake ../ && make && cp client server ../

clean:
	rm -rf build client server

zip:
	mkdir septiembre
	cp CMakeLists.txt *.cpp *.h Makefile author.txt septiembre/
	zip -r ex_666151_BARCELONA_AURIA_FEDERICO.zip septiembre
