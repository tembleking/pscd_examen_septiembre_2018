#include <iostream>
#include <thread>
#include "TcpSocket.h"
#include "WaitGroup.h"
#include "Types.h"
#include "MonitorMatrix.h"

void handleClient(TcpSocket client, MonitorMax &monitorMax, WaitGroup &wg) {
    Packet packet;
    while (client.Valid()) {

        if (!client.Receive(packet)) {
            break;
        }

        ClientRequest request{};
        request.fromPacket(packet);


        monitorMax.updateMax(request.maxFound);

        ServerResponse response;
        response.response = "OK";
        packet = response.toPacket();

        if (!client.Send(packet)) {
            break;
        }

    }
    wg.Done();
}

int main() {
    const uint16_t port = PORT;

    TcpSocket server;
    if (!server.Listen("0.0.0.0", port)) {
        std::cerr << "Could not listen on port " << port << std::endl;
        exit(1);
    }

    WaitGroup wg;
    MonitorMax monitorMax;

    unsigned int numClients = 0;

    while (server.Valid() && numClients < 3) {
        auto tcpSocket = server.Accept();
        std::thread thread(handleClient, tcpSocket, std::ref(monitorMax), std::ref(wg));
        wg.Add(1);
        numClients++;
        thread.detach();
    }

    wg.Wait();

    std::cout << "Todos los procesos han calculado, que el max es: " << monitorMax.getMax() << std::endl;

    server.Close();


}