#ifndef TYPES_H
#define TYPES_H

#include <cstdint>
#include <string>
#include <ostream>
#include "Packet.h"

#define PORT 4000

struct ClientRequest {
    uint8_t processId;
    uint8_t processedRow;
    int64_t maxFound;

    void fromPacket(Packet packet) {
        packet >> processId;
        packet >> processedRow;
        packet >> maxFound;
    }

    Packet toPacket() {
        Packet packet;
        packet << processId;
        packet << processedRow;
        packet << maxFound;
        return packet;
    }

    friend std::ostream &operator<<(std::ostream &os, const ClientRequest &request) {
        os << "processId: " << (int) request.processId << " processedRow: " << (int) request.processedRow << " maxFound: "
           << request.maxFound;
        return os;
    }
};

struct ServerResponse {
    std::string response;

    void fromPacket(Packet packet) {
        packet >> response;
    }

    Packet toPacket() {
        Packet packet;
        packet << response;
        return packet;
    }
};

typedef std::vector<std::vector<int>> Matrix;

Matrix NewSquareMatrix(unsigned int size) {
    Matrix matrix;

    for (int i = 0; i < size; ++i) {
        std::vector<int> row;
        row.reserve(size);
        for (int j = 0; j < size; ++j) {
            row.emplace_back(i + j);
        }
        matrix.emplace_back(row);
    }

    return matrix;
}


#endif //TYPES_H
