#include <thread>
#include <iostream>
#include <iomanip>
#include "TcpSocket.h"
#include "MonitorMatrix.h"
#include "Types.h"

#define MATRIX_SIZE 10

void Process(uint8_t id, const Matrix &matrix, std::string address, uint16_t port, MonitorMatrix &monitorMatrix) {
    TcpSocket client;

    // Todo: ARGS for this
    if (!client.Connect(address, port)) {
        std::cerr << "Could not connect to " << address << " on port " << port << std::endl;
        return;
    }

    while (true) {
        int nextMatrixIndex = monitorMatrix.getNextRowToCheck();

        if (nextMatrixIndex < 0 || nextMatrixIndex >= matrix.size()) {
            break;
        }

        int64_t localMax = matrix[nextMatrixIndex][0];
        for (int i : matrix[nextMatrixIndex]) {
            if (i > localMax) {
                localMax = i;
            }
        }

        // Enviamos la info al server
        ClientRequest request;
        request.maxFound = localMax;
        request.processedRow = static_cast<uint8_t>(nextMatrixIndex);
        request.processId = id;
        auto packet = request.toPacket();


        client.Send(packet);

        packet.clear();
        client.Receive(packet);

        ServerResponse response;
        response.fromPacket(packet);
    }

    client.Close();
}

int main() {
    Matrix matrix = NewSquareMatrix(MATRIX_SIZE);
    MonitorMatrix monitorMatrix(MATRIX_SIZE);


    std::string address = "localhost";
    uint16_t port = PORT;


    std::thread thread1(Process, 1, std::ref(matrix), address, port, std::ref(monitorMatrix));
    std::thread thread2(Process, 2, std::ref(matrix), address, port, std::ref(monitorMatrix));
    std::thread thread3(Process, 3, std::ref(matrix), address, port, std::ref(monitorMatrix));

    thread1.join();
    thread2.join();
    thread3.join();
}