#ifndef FILAS_PENDIENTES_H
#define FILAS_PENDIENTES_H

#include <condition_variable>
#include <vector>

class MonitorMatrix {
    std::mutex mtx;

    std::vector<bool> filas_pendientes;

public:
    explicit MonitorMatrix(unsigned int size) {
        for (int i = 0; i < size; ++i) {
            filas_pendientes.emplace_back(true);
        }
    }

    /**
     * @return Returns the index of the next row to check, or -1 if there's no one left.
     */
    int getNextRowToCheck() {
        std::unique_lock<std::mutex> lck(mtx);

        for (int i = 0; i < filas_pendientes.size(); ++i) {
            if (filas_pendientes[i]) {
                filas_pendientes[i] = false;
                return i;
            }
        }
        return -1; // no more rows to check
    }
};

class MonitorMax {
    std::mutex mtx;

    int64_t maxFound;

public:
    MonitorMax() : maxFound(0x8000000000000000) { // Min negative number of 64 bits
    }

    void updateMax(int64_t max) {
        std::unique_lock<std::mutex> lck(mtx);

        if (max > maxFound) {
            maxFound = max;
        }
    }

    int64_t getMax() {
        std::unique_lock<std::mutex> lck(mtx);
        return maxFound;
    }

};

#endif //FILAS_PENDIENTES_H
