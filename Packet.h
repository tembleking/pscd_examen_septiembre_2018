
#ifndef PACKETBUFFER_H
#define PACKETBUFFER_H

#include <cstdint>
#include <vector>
#include <netinet/in.h>
#include <cstring>
#include <string>

class Packet {
    std::vector<uint8_t> buffer;
    size_t readIndex;
public:
    Packet() : readIndex(0) {}

    Packet(const uint8_t *buff, size_t size) : readIndex(0) {
        addRaw(buff, size);
    }

    //-----------------------------------------
    Packet &operator<<(uint8_t data) {
        buffer.emplace_back(data);
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(int8_t data) {
        buffer.emplace_back(data);
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(uint16_t data) {
        uint8_t b[2];
        uint16_t datan = htons(data);
        memcpy(b, &datan, 2);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(int16_t data) {
        uint8_t b[2];
        int16_t datan = htons(static_cast<uint16_t>(data));
        memcpy(b, &datan, 2);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(uint32_t data) {
        uint8_t b[4];
        uint32_t datan = htonl(data);
        memcpy(b, &datan, 4);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(int32_t data) {
        uint8_t b[4];
        int32_t datan = htonl(static_cast<uint32_t>(data));
        memcpy(b, &datan, 4);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(uint64_t data) {
        uint8_t b[8];
        uint64_t datan = hton64(data);
        memcpy(b, &datan, 8);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(int64_t data) {
        uint8_t b[8];
        int64_t datan = hton64(static_cast<uint64_t>(data));
        memcpy(b, &datan, 8);
        for (uint8_t c : b) {
            buffer.emplace_back(c);
        }
        return *this;
    }

    //-----------------------------------------
    Packet &operator<<(std::string data) {
        for (char &c : data) {
            buffer.emplace_back(c);
        }
        buffer.emplace_back('\0');
        return *this;
    }

    //=========================================
    Packet &operator>>(uint8_t &data) {
        uint8_t deserialized = buffer.at(readIndex++);
        data = deserialized;
        return *this;
    }

    //-----------------------------------------
    Packet &operator>>(int8_t &data) {
        uint8_t deserialized = buffer.at(readIndex++);
        data = deserialized;
        return *this;
    }

    //-----------------------------------------
    Packet &operator>>(uint16_t &data) {
        uint8_t b[2];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        uint16_t deserialized;
        memcpy(&deserialized, b, 2);
        data = ntohs(deserialized);
        return *this;
    }

    //------------------------------------------
    Packet &operator>>(int16_t &data) {
        uint8_t b[2];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        int16_t deserialized;
        memcpy(&deserialized, b, 2);
        data = ntohs(static_cast<uint16_t>(deserialized));
        return *this;
    }

    //-----------------------------------------
    Packet &operator>>(uint32_t &data) {
        uint8_t b[4];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        uint32_t deserialized;
        memcpy(&deserialized, b, 4);
        data = ntohl(deserialized);
        return *this;
    }

    //-----------------------------------------
    Packet &operator>>(int32_t &data) {
        uint8_t b[4];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        int32_t deserialized;
        memcpy(&deserialized, b, 4);
        data = ntohl(static_cast<uint32_t>(deserialized));
        return *this;
    }

    //------------------------------------------
    Packet &operator>>(uint64_t &data) {
        uint8_t b[8];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        uint64_t deserialized;
        memcpy(&deserialized, b, 8);
        data = ntoh64(deserialized);
        return *this;
    }

    //------------------------------------------
    Packet &operator>>(int64_t &data) {
        uint8_t b[8];
        for (uint8_t &i : b) {
            i = buffer.at(readIndex++);
        }
        int64_t deserialized;
        memcpy(&deserialized, b, 8);
        data = ntoh64(static_cast<uint64_t>(deserialized));
        return *this;
    }

    //-----------------------------------------
    Packet &operator>>(std::string &data) {
        std::string deserialized;
        for (int i = 0; '\0' != buffer[readIndex]; ++i, readIndex++) {
            deserialized += buffer[readIndex];
        }
        readIndex++;
        data = deserialized;
        return *this;
    }

    //-----------------------------------------
    void addRaw(const uint8_t *buff, size_t size) {
        if (buff) {
            for (size_t i = 0; i < size; ++i) {
                buffer.emplace_back(buff[i]);
            }
        }
    }

    size_t size() const {
        return buffer.size();
    }

    const uint8_t *data() const {
        return buffer.data();
    }

    bool empty() const {
        return buffer.empty();
    }

    void clear() {
        reset();
        buffer.clear();
    }

    void reset() {
        readIndex = 0;
    }

private:
    uint64_t ntoh64(uint64_t input) {
        uint64_t rval;
        auto *data = (uint8_t *) &rval;

        data[0] = static_cast<uint8_t>(input >> 56);
        data[1] = static_cast<uint8_t>(input >> 48);
        data[2] = static_cast<uint8_t>(input >> 40);
        data[3] = static_cast<uint8_t>(input >> 32);
        data[4] = static_cast<uint8_t>(input >> 24);
        data[5] = static_cast<uint8_t>(input >> 16);
        data[6] = static_cast<uint8_t>(input >> 8);
        data[7] = static_cast<uint8_t>(input >> 0);

        return rval;
    }

    uint64_t hton64(uint64_t input) {
        return ntoh64(input);
    }

};

#endif //PACKETBUFFER_H
