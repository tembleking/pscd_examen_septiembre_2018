#ifndef TCPSOCKET_H

#define TCPSOCKET_H

#include <string>
#include <utility>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <netdb.h>
#include <strings.h>
#include "Packet.h"

class TcpSocket {
    int socket_fd;

private:

    void Create() {
        //                      IPv4     TCP
        socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    }

    bool Bind(std::string address, uint16_t port) {

        struct sockaddr_in server{};

        server.sin_family = AF_INET; // IPv4
        server.sin_port = htons(port);
        server.sin_addr.s_addr = ResolveAddress(std::move(address));
        bzero(&(server.sin_zero), 8);

        // Llamada a bind
        return ::bind(socket_fd, reinterpret_cast<sockaddr *>(&server), sizeof(struct sockaddr_in)) == 0; // success
    }

    in_addr_t ResolveAddress(std::string address) {
        if (address == "255.255.255.255") return INADDR_BROADCAST;
        if (address == "0.0.0.0") return INADDR_ANY;

        in_addr_t ip = inet_addr(address.c_str());
        if (ip != INADDR_NONE) return ip;

        addrinfo hints{};
        std::memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;
        addrinfo *result = nullptr;

        if (getaddrinfo(address.c_str(), NULL, &hints, &result) == 0) {
            if (result) {
                ip = reinterpret_cast<sockaddr_in *>(result->ai_addr)->sin_addr.s_addr;
                freeaddrinfo(result);
                return ip;
            }
        }

        return INADDR_ANY; // Listen in all addresses
    }

    explicit TcpSocket(int socket_fd) : socket_fd(socket_fd) {}

public:
    TcpSocket() : socket_fd(-1) {}


    bool Listen(std::string ip_address, uint16_t port, uint8_t max_connections = SOMAXCONN) {
        Close();
        Create();

        if (!Bind(ip_address, port)) return false;

        return (::listen(socket_fd, max_connections) == 0);
    }

    bool Valid() const {
        return socket_fd >= 0;
    }

    void Close() {
        close(socket_fd);
        socket_fd = -1;
    }

    TcpSocket Accept() {
        struct sockaddr_in client{};  // Información dirección scliente
        socklen_t sin_size = sizeof(struct sockaddr_in);
        int fd = ::accept(socket_fd, reinterpret_cast<sockaddr *>(&client), &sin_size);

        return TcpSocket(fd);
    }


    bool Connect(std::string address, uint16_t port) {
        Close();
        Create();

        struct hostent *he;
        struct sockaddr_in server{};

        he = gethostbyname(address.c_str()); // Get server address
        if (he == nullptr) return false;

        server.sin_family = AF_INET;
        server.sin_port = htons(port);
        server.sin_addr = *(reinterpret_cast<in_addr *>(he->h_addr));
        bzero(&(server.sin_zero), 8);

        return ::connect(socket_fd, (struct sockaddr *) &server, sizeof(struct sockaddr)) == 0;
    }

    bool Send(Packet packet) {
        if (packet.size() == 0 || packet.data() == nullptr) return false;

        Packet newPacket;
        newPacket << (uint64_t) (packet.size());
        newPacket.addRaw(packet.data(), packet.size());


        auto size = newPacket.size();
        auto data = newPacket.data();

        // Loop until every byte has been sent
        for (size_t sent = 0, result = 0; sent < size; sent += result) {
            // Send a chunk of data
            result = ::send(socket_fd, data + sent, size - sent, 0);

            if (result < 0) return false;
        }
        ::send(socket_fd, data, 0, 0);

        return true;
    }

    bool Receive(Packet &packet) {
        packet.clear();

        Packet packetSizep;
        uint64_t packetSize = 0;
        uint8_t bufferForSize[sizeof(uint64_t)];
        auto sizeRead = ::recv(socket_fd, bufferForSize, sizeof(uint64_t), 0); // receive the size of the package
        if (sizeRead <= 0) return false; // socket closed

        packetSizep.addRaw(bufferForSize, sizeof(uint64_t));
        packetSizep >> packetSize;

        uint8_t buffer[packetSize];

        auto size_recv = recv(socket_fd, buffer, packetSize, 0);

        packet.addRaw(buffer, size_recv);

        return size_recv > 0;
    }
};

#endif //TCPSOCKET_H
