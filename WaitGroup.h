#ifndef WAITGROUP_H
#define WAITGROUP_H

#include <mutex>
#include <condition_variable>

class WaitGroup {
    typedef std::unique_lock<std::mutex> lock;
    std::mutex mtx;
    std::condition_variable cv;
    unsigned int count;
public:
    void Add(unsigned int qty) {
        lock lck(mtx);
        count += qty;
    }

    void Done() {
        lock lck(mtx);
        count--;
        cv.notify_all();
    }

    void Wait() {
        lock lck(mtx);
        cv.wait(lck, [this]() { return count == 0; });
    }
};

#endif //WAITGROUP_H
